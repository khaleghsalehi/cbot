#!/bin/bash
rm *pdf
rm *log
rm cbot
rm run >
clear
gcc -I /usr/include -pthread -Wall -ansi -pedantic -ggdb \
  lib/cbot.c -o cbot -lwkhtmltox \
  -Wl,-rpath,/usr/local/lib \
  -D THPOOL_DEBUG
gcc executer.c -lpthread -o run
