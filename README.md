# cbot
How to use it:

1- get the version of wkhtmltopdf
    https://wkhtmltopdf.org/downloads.html

2- run ./compile.sh

3- edit cbot.cfg and put target

4- finally run cbot
    e.g. ./run 100

# How to launch cbot in headless server?

well, one of the easy, low cost solution is a xvfb.
e.g sudo xvfb-run ./run 100

