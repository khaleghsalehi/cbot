#include  <stdio.h>
#include <stdlib.h>
#include "lib/thpool.c"

#define  LINE_MAX 256
#define  CONFIG_FILE "cbot.cfg"

void runbot() {
    char cmd_line[LINE_MAX];
    FILE *file;
    file = fopen(CONFIG_FILE, "r");
    if (file) {
        while (fgets(cmd_line, sizeof cmd_line, file) != NULL);
        fclose(file);
        system(cmd_line);
    } else {
        printf("error reading cbot.cfg.");
        exit(EXIT_FAILURE);
    }

}

void print_banner() {
    printf("\n");
    printf("\t ██████╗██████╗  ██████╗ ████████╗\n");
    printf("\t██╔════╝██╔══██╗██╔═══██╗╚══██╔══╝\n");
    printf("\t██║     ██████╔╝██║   ██║   ██║   \n");
    printf("\t██║     ██╔══██╗██║   ██║   ██║   \n");
    printf("\t╚██████╗██████╔╝╚██████╔╝   ██║   \n");
    printf("\t ╚═════╝╚═════╝  ╚═════╝    ╚═╝   \n");
    printf("\tversion 0.0.4\n");
    printf("\thttps://innovera.ir\n\n");

}

int main(int argc, char *argv[]) {
    print_banner();
    if (argc <= 1) {
        printf("Usage:\n%s concurrent\nexample: %s 100\n",
               argv[0],
               argv[0]);
        return -1;
    }
    printf("making threadpool with %d threads\n", atoi(argv[1]));
    threadpool thpool = thpool_init(atoi(argv[1]));
    thpool_wait(thpool);


    puts("add tasks to threadpool\n");
    int i;
    for (i = 0; i < atoi(argv[1]); i++) {
        thpool_add_work(thpool, (void *) runbot, NULL);
    }

    thpool_destroy(thpool);

    return 0;


}
